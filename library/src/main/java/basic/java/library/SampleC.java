package basic.java.library;

public class SampleC {
    static int year = 1397;
    static int price = 3000000;
    static String address = "Sar Afraz, Beheshti";
    static boolean expensive = true;

    public static void main(String[] args) {
        printYear(2018);
        printYear(2080);
        printYear(2030);
        printYear(2035);
        print("my name is soheil");

    }

    static void printYear(int year) {
        print("this year is " + year);
    }

    static void print(String msg) {
        System.out.println(msg);
    }
}
