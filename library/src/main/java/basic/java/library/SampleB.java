package basic.java.library;

public class SampleB {
    public static void main(String[] args) {
        printMyName();
    }

    static String name = "Soheil";
    static String family = "Afzali";
    static int myAge = 27;

    static void printMyName() {
        System.out.println("my name is " + name);
        System.out.println(family);
        System.out.println(name + " " + family);
        System.out.println(name + " " + family + " " + myAge);

    }
}
