package basic.java.library;

public class VariableSample {
    boolean iAmAlive = true;
    String name = "Soheil";
    String family = "Afzali";
    int age = 27;
    boolean isIranian = true;
    double pNumber = 3.144444444;
    float latitute = 35.123456f;

    String getMyName() {
        return "Soheil";
    }

    boolean isAlive() {
        return iAmAlive;
    }

    int getUserAge(int userId) {
        return userId + 10;
    }

    String getUser(int id, boolean isIranian) {
        if (id == 10 && isIranian == true) {
            return "amir";
        } else
            return "javad";
    }
    void printUser(){
        String user=getUser(10,true);
    }
}
