package basic.java.library;

public class VaribleStatment {
    public static void main(String[] args) {
        getTypeByAge(20);
        getPriceByCarName("pride");

    }

    static String getTypeByAge(int age) {
        if (age <= 10) {
            return "Child";
        }
        if (age > 10 && age <= 19) {
            return "teenAge";
        }
        if (age > 19 && age <= 40) {
            return "young";
        }
        if (age > 40) {
            return "old";
        }
        return " ";
    }

    static int getPriceByCarName(String car) {
        if (car == "pride") {
            return 20000000;
        } else {
            return 0;
        }
    }

    boolean isInIran(String city) {
        if (city == "tehran") {
            return true;
        } else if (city == "dubai") {
            return false;
        } else if (city == "tabriz") {
            return true;
        } else {
            return false;
        }
    }

}
